{{- if .Values.enabled -}}
apiVersion: extensions/v1beta1
kind: Deployment
metadata:
  name: {{ template "fullname" . }}
  labels:
    app: {{ template "name" . }}
    chart: {{ .Chart.Name }}-{{ .Chart.Version | replace "+" "_" }}
    release: {{ .Release.Name }}
    heritage: {{ .Release.Service }}
spec:
  replicas: {{ .Values.replicaCount }}
  template:
    metadata:
      labels:
        app: {{ template "name" . }}
        release: {{ .Release.Name }}
      annotations:
        checksum/config: {{ include (print $.Template.BasePath "/configmap.yml") . | sha256sum }}
    spec:
      initContainers:
        - name: configure
          command: ['sh', '/config/configure']
          image: busybox
          volumeMounts:
          - name: gitaly-config
            mountPath: /config
            readOnly: true
          - name: init-gitaly-secrets
            mountPath: /init-config
            readOnly: true
          - name: gitaly-secrets
            mountPath: /init-secrets
            readOnly: false
      securityContext:
        runAsUser: 1000
        fsGroup: 1000
      containers:
        - name: {{ .Chart.Name }}
          image: "{{ .Values.image.repository }}:{{ .Values.image.tag }}"
          imagePullPolicy: {{ .Values.image.pullPolicy }}
          ports:
            - containerPort: {{ .Values.service.internalPort }}
          env:
            - name: CONFIG_TEMPLATE_DIRECTORY
              value: '/etc/gitaly/templates'
            - name: CONFIG_DIRECTORY
              value: '/etc/gitaly'
            - name: GITALY_CONFIG_FILE
              value: '/etc/gitaly/config.toml'
          volumeMounts:
            - name: gitaly-config
              mountPath: '/etc/gitaly/templates'
            - name: gitaly-secrets
              mountPath: '/etc/gitlab-secrets'
              readOnly: true
            - name: repo-data
              mountPath: '/home/git/repositories'
              {{- if and .Values.persistence.enabled .Values.persistence.subPath }}
              subPath: "{{ .Values.persistence.subPath }}"
              {{- end }}
          livenessProbe:
            exec:
              command:
              - /scripts/healthcheck
            initialDelaySeconds: 30
            timeoutSeconds: 3
            periodSeconds: 10
          readinessProbe:
            exec:
              command:
              - /scripts/healthcheck
            initialDelaySeconds: 10
            timeoutSeconds: 3
            periodSeconds: 10
          resources:
{{ toYaml .Values.resources | indent 12 }}
      volumes:
      - name: gitaly-config
        configMap:
          name: {{ template "fullname" . }}
      - name: repo-data
      {{- if .Values.persistence.enabled }}
        persistentVolumeClaim:
          claimName: {{ template "fullname" . }}-repos
      {{- else }}
        emptyDir: {}
      {{- end }}
      - name: gitaly-secrets
        emptyDir:
          medium: "Memory"
      - name: init-gitaly-secrets
        projected:
          defaultMode: 0440
          sources:
          - secret:
              name: {{ .Values.authToken.secret }}
              items:
                - key: {{ .Values.authToken.key }}
                  path: "gitaly_token"
          - secret:
              name: {{ .Values.shell.authToken.secret }}
              items:
                - key: {{ .Values.shell.authToken.key }}
                  path: ".gitlab_shell_secret"
          - secret:
              name: {{ .Values.redis.password.secret }}
              items:
                - key: {{ .Values.redis.password.key }}
                  path: redis_password
    {{- if .Values.nodeSelector }}
      nodeSelector:
{{ toYaml .Values.nodeSelector | indent 8 }}
    {{- end }}
{{- end }}
